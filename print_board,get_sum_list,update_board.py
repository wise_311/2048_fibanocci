#printing board when called

def print_board(board):
    for i in range(len(board)):
        col = ""
        for j in range(len(board[0])):
            col += str(board[i][j])+ " "
        print (col)
        
#Adding elements to sum_list

def get_sum_list(arr,fib_dict):
    adjelem1 = -1
    sum_list = []
    for elem in arr:
        if elem:
            if adjelem1 == -1:
                adjelem1 = elem
            else:
                if (adjelem1 + elem) in fib_dict["fib_map"]:
                    sum_list.append(adjelem1 + elem)
                    adjelem1 = -1
                else:
                    sum_list.append(adjelem1)
                    adjelem1 = elem

    if adjelem1 != -1:
        sum_list.append(adjelem1)
    return sum_list

#board is updated for every iteration

def update_board(board,strp,upd_list,dir):
    k = 0
    rows = len(board)
    cols = len(board[0])
    upd_len = len(upd_list)
    if dir == "s":
        row = rows-1
        while row>=0:
            if k < upd_len:
                board[row][strp] = upd_list[k]
                k += 1
            else:
                board[row][strp] = 0
            row -=1
    elif dir == "w":
        row = 0
        while row<rows:
            if k < upd_len:
                board[row][strp] = upd_list[k]
                k += 1
            else:
                board[row][strp] = 0
            row+=1
    elif dir == "d":
        col = cols-1
        while col>=0:
            if k < len(upd_list):
                board[strp][col] = upd_list[k]
                k += 1
            else:
                board[strp][col] = 0
            col -=1
    if dir == "a":
        col = 0
        while col<cols:
            if k < len(upd_list):
                board[strp][col] = upd_list[k]
                k += 1
            else:
                board[strp][col] = 0
            col += 1
