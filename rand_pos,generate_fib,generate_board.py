from random import randint

#creation of random number

def rand_pos(game,rand_1=1):
    x = []
    y = []
    for i in range (len(game)):
        for j in range(len(game[0])):
            if game[i][j] == 0:
               x.append(i)
               y.append(j)
    if not x:
        return None

    for i in range(rand_1):
        if not x:
            break
        rand_pos = randint(0, len(x)-1)
        game[x[rand_pos]][y[rand_pos]] = 1
        del x[rand_pos]
        del y[rand_pos]
    return game

#Board is created with  0 first

def generate_board(m,n):
    board = [[0 for x in range(n)] for y in range(m)]
    for i  in range (m):
        for j in range(n):
            board[i][j] = 0
    return board

# main logic of code FIBONACCI SERIES

def generate_fib(m,n):
    term = m*n
    fib = []
    fib.append(1)
    fib.append(1)
    #reverse map created
    fib_map = {}
    for i in range(2,term):
        fib.append(fib[i-1] + fib[i-2])
        fib_map[fib[i]] = i
    return {"fib_series":fib,"fib_map":fib_map}
