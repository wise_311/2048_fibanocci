
#direction updation  for every iteration

def mov_dir(dir,board,fib_dict):
    cols = len(board[0])
    rows = len(board)
    if dir == "w" or dir == "s":
        for j in range(cols):
            col_arr = []
            for i in range(rows):
                col_arr.append(board[i][j])
            if dir == "s":
                col_arr.reverse()
            upd_list = get_sum_list(col_arr,fib_dict)
            update_board(board,j,upd_list,dir)

    elif dir == "a" or dir == "d":
        for i in range(rows):
            col_arr=[]
            for j in range(cols):
                col_arr.append(board[i][j])
            if dir == "d":
                col_arr.reverse()
            # print col_arr
            upd_list = get_sum_list(col_arr,fib_dict)
            update_board(board,i,upd_list,dir)

#checking winning condition

def check_win(board,fib_dict):
    for i in range(len(board)):
        for j in range(len(board[0])):
            if board[i][j] == fib_dict["fib_series"][-1]:
                return 1
    return 0

#inputs  of the code
board = generate_board(4, 4)
fib_dict = generate_fib(4, 4)
rand_pos(board,2)
print ("Initial board state")
print_board(board)

dir_moves = ["w","s","a","d"]
if check_win(board, fib_dict):
    print ("You won")
    exit()
while True:
    dir = input(" Enter the direction(w-up/s-down/a-left/d-right). For exiting, enter exit.")
    dir = dir.lower()
    if dir == "exit":
        break
    elif dir in dir_moves:
        mov_dir(dir, board, fib_dict)
    else:
        print ("Please enter a valid move")
        print_board(board)
        continue
    if check_win(board,fib_dict):
        print ("You won")
        break

    if not rand_pos(board):
        print ("Game lost")
        break
    print_board(board)

print_board(board)
print ("Thank you")
